(function() {
	var Helpers;
	
	Helpers = (function() {
		function Helpers() {}
		
		Helpers.prototype.createDiv = function(o) {
			var $cont, $el;
			if (o == null) {
				o = {};
			}
			$el = $('<div />');
			(o['id'] != null) && $el.attr('id', o['id']);
			(o['class'] != null) && $el.addClass(o['class']);
			$cont = (o != null ? o.container : void 0) || $(document.body);
			$cont.append($el);
			return $el;
		};
		return Helpers;
	})();
	window.helpers = new Helpers;
}).call(this);

function MovePlayer0($wrap) {
	this.$wrap = $wrap;
	this.isPlayLog = false;
}
MovePlayer0.prototype.start = function() {
	if (this.isPlayLog) return;
	this.isPlayLog = true;
	
	var $page1 = helpers.createDiv({ 'class': 'page1', container: this.$wrap }),
		$page2 = helpers.createDiv({ 'class': 'page2', container: this.$wrap }),
		$sticker1 = helpers.createDiv({ 'class': 'sticker1', container: this.$wrap }),
		$sticker2 = helpers.createDiv({ 'class': 'sticker2', container: this.$wrap }),
		elements = this.$wrap.children();
	
	var seq = [
		{ e: $sticker1, p: { opacity: 1 }, o: { duration: 0, delay: 1000 } },
		{ e: $sticker2, p: { opacity: 1 }, o: { duration: 0, delay: 200, sequenceQueue: false } },
		{ e: $page1, p: { opacity: 1 }, o: { duration: 0, delay: 0, sequenceQueue: false } },
		{ e: $page1, p: { marginLeft: '-10.1875rem', marginTop: '2.34375rem' }, o: { duration: 500, delay: 0, sequenceQueue: false } },
		{ e: $page2, p: { opacity: 1,  marginLeft: '-0.46875rem', marginTop: '1.15625rem' }, o: { duration: 800, delay: 0, sequenceQueue: false } },
	];
	
	elements.velocity('stop');
	$.Velocity.RunSequence(seq);
}
MovePlayer0.prototype.stop = function () {
	this.$wrap.children().remove();
}

function MovePlayer1($wrap) {
	this.$wrap = $wrap;
	this.isPlayLog = false;
}
MovePlayer1.prototype.start = function() {
	if (this.isPlayLog) return;
	this.isPlayLog = true;
	
	var $phone = helpers.createDiv({ 'class': 'phone', container: this.$wrap }),
		$panner = helpers.createDiv({ 'class': 'panner', container: this.$wrap }),
		$view1 = helpers.createDiv({ 'class': 'view1', container: $panner }),
		$view1Touch = helpers.createDiv({ 'class': 'view1_touch', container: $panner }),
		$wrapPlayer = helpers.createDiv({ 'class': 'wrap_player', container: $panner }),
		$video = $('<video height="100%"  muted playsinline poster="./image/scean_02_video_poster.jpg"><source src="./image/scean_02_video.mp4" type="video/mp4">지원하지 않는 브라우저 입니다.</video>'),
		$hand = helpers.createDiv({ 'class': 'hand', container: this.$wrap }),
		elements = this.$wrap.children();
	
	var seq = [
		{ e: $panner, p: { opacity: 1 }, o: { duration: 0, delay: 0 } },
		{ e: $view1Touch, p: { opacity: 1 }, o: { duration: 0, delay: 200, complete: function () {
			$video[0].controls = false;
			$video[0].loop = false;
			$wrapPlayer.prepend($video);
		} } },
		{ e: $wrapPlayer, p: { opacity: 1 }, o: { duration: 300, delay: 500, complete: function () {
			$video[0].play();
		} } },
		{ e: $hand, p: { opacity: 1, top: '14.6875rem' }, o: { duration: 0, delay: 1000 } },
		{ e: $hand, p: { scale : 0.99 }, o: { duration: 0 } },
		{ e: $view1, p: { marginTop : '-22.8125rem' }, o: { duration: 1500, sequenceQueue: false } },
		{ e: $wrapPlayer, p: { marginTop : '-22.8125rem' }, o: { duration: 1500, sequenceQueue: false } },
		{ e: $hand, p: { top: '-=10.2rem', opacity: 1 }, o: { duration: 1000, easing: 'easeOutSine', sequenceQueue: false } },
	];
	
	elements.velocity('stop');
	$.Velocity.RunSequence(seq);
}
MovePlayer1.prototype.stop = function () {
	this.$wrap.children().remove();
}

function MovePlayer2($wrap) {
	this.$wrap = $wrap;
	this.isPlayLog = false;
}
MovePlayer2.prototype.start = function() {
	if (this.isPlayLog) return;
	this.isPlayLog = true;
	
	var $phone = helpers.createDiv({ 'class': 'phone', container: this.$wrap }),
		$panner = helpers.createDiv({ 'class': 'panner', container: this.$wrap }),
		$controls = helpers.createDiv({ 'class': 'controls', container: $panner }),
		$controls2 = helpers.createDiv({ 'class': 'controls2', container: $panner }),
		$wrapPlayer = helpers.createDiv({ 'class': 'wrap_player', container: $panner }),
		$video = $('<video height="100%" muted playsinline><source src="./image/scean_03_video.mp4" type="video/mp4">지원하지 않는 브라우저 입니다.</video>'),
		$hand = helpers.createDiv({ 'class': 'hand', container: this.$wrap }),
		elements = this.$wrap.children();
	
	$video[0].controls = false;
	$video[0].loop = false;
	$video[0].currentTime = 0;
	$wrapPlayer.prepend($video);

	var seq = [
		{ e: $phone, p: { opacity: 1 }, o: { duration: 600, delay: 300, easing:'easeOutSine' } },
		{ e: $hand, p: { opacity: 1 }, o: { duration: 0, delay: 500 } },
		{ e: $hand, p: { scale : 0.8 }, o: { duration: 300, delay: 300 } },
		{ e: $hand, p: { scale : 1, opacity: 0 }, o: { duration: 100 } },
		{ e: $controls, p: { opacity: 0 }, o: { duration: 100, delay: 0, complete:function () {
			setTimeout(function () {
				$video[0].play();
			}, 100);
		} } },
		{ e: $hand, p: { opacity: 1 }, o: { duration: 0, delay: 5000 } },
		{ e: $hand, p: { top: '20.375rem', marginLeft: '-6.15625rem' }, o: { duration: 700, delay: 0 } },
		{ e: $hand, p: { scale : 0.8 }, o: { duration: 300, delay: 300 } },
		{ e: $hand, p: { scale : 1, opacity: 0 }, o: { duration: 100 } },
		{ e: $controls2, p: { marginTop: '-11.75rem' }, o: { duration: 300, easing: 'easeOutSine', sequenceQueue: false } },
	];
	
	elements.velocity('stop');
	$.Velocity.RunSequence(seq);
}

MovePlayer2.prototype.stop = function () {
	animationStopController(this);
}

function MovePlayer3($wrap) {
	this.$wrap = $wrap;
	this.isPlayLog = false;
}

MovePlayer3.prototype.start = function() {
	if (this.isPlayLog) return;
	this.isPlayLog = true;
	
	var $phone = helpers.createDiv({ 'class': 'phone', container: this.$wrap }),
		$wrap_slider = helpers.createDiv({ 'class': 'wrap_slider', container: $phone }),
		$slider = helpers.createDiv({ 'class': 'slider', container: $wrap_slider }),
		$hand = helpers.createDiv({ 'class': 'hand', container: this.$wrap }),
		elements = this.$wrap.children();
	
	var seq = [
		{ e: $hand, p: { opacity: 1 }, o: { duration: 0, delay: 1000 } },
		{ e: $hand, p: { top: '18.03125rem', marginLeft: '4.375rem' }, o: { duration: 500, delay: 1000 } },
		{ e: $hand, p: { scale : 0.9 }, o: { duration: 300 } },
		{ e: $hand, p: { scale : 1, delay: 200 }, o: { duration: 100 ,complete: function () {
			$hand.velocity('transition.fadeOut');
			setTimeout(function () {
				document.querySelector('.wrap_slider').classList.add('on');
			}, 300);
		} } },
	];
	
	elements.velocity('stop');
	$.Velocity.RunSequence(seq);
}

MovePlayer3.prototype.stop = function () {
	animationStopController(this);
}

function animationStopController(_this) {
	function reset() {
		_this.$wrap.children().fadeOut('2000', function () {
			$(this).remove();
		});
	}
	$.when(reset()).done(function() {
		if (_this.$wrap.hasClass('appear')){
			_this.start();
		}
	});
}

$(function(){
	var trigger = new ScrollTrigger({
		once: true,
		addHeight: true
	});
	
	var playList = new Array(
		null,
		new MovePlayer0($('#animation01')),
		new MovePlayer1($('#animation02')),
		new MovePlayer2($('#animation03')),
		new MovePlayer3($('#animation04')),
	);
	
	function product_detail_apear(item){
		$('#area'+ item + ' .frame').addClass('appear');
		if(playList[item] !== null) playList[item].start();
	}
	
	function product_detail_disappear(item) {
		$('#area'+ item + ' .frame').removeClass('appear');
		if(playList[item] !== null) playList[item].stop();
	}
	
	$.each(playList, function(index, item){
		if(item !== null) {
			$('#area'+ index + ' .frame' ).on('scrollSpy:enter', function() {
				product_detail_apear(index);
			});
			$('#area'+index + ' .frame').scrollSpy();
		}
	});
	
});
